```bash
├── Project Name
│   ├── Hardware
│   │   ├── Meca
│   │   |   ├── Bom
│   │   |   ├── Freecad
│   │   ├── Elec
│   │   |   ├── Bom
│   │   |   ├── Kicad
│   ├── Software
│   │   ├── Embedded
│   │   |   ├── Circuitpython
│   │   |   ├── Arduino
│   │   |   ├── (...)
```
